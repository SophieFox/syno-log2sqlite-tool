import os, sys
from datetime import datetime
from dateutil import parser as dateparser

from pony.orm import (
    Database,
    PrimaryKey,
    Required,
    Optional,
    db_session
)

class LogEntry(db.Entity):
    id = PrimaryKey(int, auto=True)
    ts = Required(datetime)
    level = Required(str)
    hostname = Required(str)
    category = Required(str)
    program  = Optional(str)
    messages = Optional(str)


@db_session
def parse_csv(ifilepath):

    def parse_line(line):
        if line == '': return False
        
        parts = line.strip().split(',')
        if not len(parts) == 7: return True # continue on malformed line
        
        #print(parts)

        a = dict(
            ts=dateparser.parse(parts[0] + ' ' + parts[1]),
            level=parts[2],
            hostname=parts[3],
            category=parts[4],
            program=parts[5],
            messages=parts[6]
        )

        le = LogEntry(**a)
        
        return True

    with open(ifilepath, 'r') as f:
        line = f.readline()
        if parse_line(line) is False: return

        ln = 1
        while True:
            ln += 1
            line = f.readline()
            if parse_line(line) is False:
                break


if __name__ == '__main__':
    db = Database()
    db.bind(provider='sqlite', filename='output.db', create_db=True)
    db.generate_mapping(create_tables=True)

    parse_csv(sys.argv[1])

